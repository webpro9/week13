import { IsNotEmpty, Length, Min } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;

  @IsNotEmpty()
  @Min(1)
  age: number;
}
